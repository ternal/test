#Vladyslav Ternal™ - Laravel usable methods
## Endpoints

- [Auth](#Auth)
    - [attempt](#attempt)
    - [intended](#intended)
    - [check](#check)
    - [viaRemembered](#viaRemembered)
    - [loginUsingId](#loginUsingId)
    - [validate](#validate)
    - [once](#once)
    - [login](#login)
    - [logout](#logout)

###Auth
####attempt
- повертає true якщо авторизація пройшла успішно.                               
```php
Auth::attempt([                                                 
    'email' => $eamil,                                          
    'password' => $password,                                                
    'active' => 1                                                           
])                                                    
```
####intended
- повертає користувача на ту адресу, доступ до якого він хотів отримати                        
```php
return redirect()->intended('page_name');
```
####check
- перевіряє чи користувач аутентифікований
```php
Auth::check()  == true / false
```
####viaRemembered
- перевіряє чи користувач був аутентифікований за допомогою механізму запам'ятовування
```php
Auth::viaRemembered() 
```
####loginUsingId
- авторизувати юзера по айді
```php
Auth::loginUsingId($id)
```
####validate
- перевіряє права користувача без фактичної авторизації
```php
Auth::validate($crentials)
```
####once
- авторизує тільки на час виконання запиту, при цьому не використовує сесію і куки
```php
Auth::once($crentials)
```
####login <a name="login">
- авторизовує користувача
```php
Auth::login(User $user)
```
####logout <p><a name="logout"></p>
- розлоговує користувача
```php
Auth::logout(User $user)
```


